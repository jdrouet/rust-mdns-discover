use async_channel::bounded;
use async_std::stream;
use chrono::prelude::*;
use futures::{select, FutureExt};
use futures_util::{pin_mut, stream::StreamExt};
use mdns::{Error, RecordKind};
use serde::Serialize;
use std::collections::{HashMap, HashSet};
use std::io;
use std::io::prelude::*;
use std::iter::Iterator;
use std::time::{Duration, SystemTime};

// Set to true for debugging
const USE_TEXT: bool = false;

// [dependencies]
// async-channel = "1.6.1"
// async-std = "1.10.0"
// chrono = "0.4.19"
// futures = "0.3.18"
// futures-util = "0.3.18"
// mdns = "3.0.0"
// serde = { version = "1.0.130", features = ["derive"] }
// serde_json = "1.0"

// A response looks like either:
// {
//     type: 'remove',
//     service: 'Service Name'
// }
//
// Or:
// {
//     type: 'add',
//     service: 'Service Name',
//     srv: '_http._tcp'
//     hostname: 'name.local',
//     address: '192.168.10.20',
//     port: 80,
//     txt: {
//         'key1': 'Value one',
//         'key2': null,
//         ...
//         'keyN': 'Value N'
//     }
// }

#[derive(Debug, Serialize)]
#[serde(tag = "type")]
pub enum ResponseJSON {
    #[serde(rename = "remove")]
    Remove { service: String },

    #[serde(rename = "add")]
    Add {
        service: String,
        srv: String,
        hostname: String,
        address: String,
        port: u16,
        txt: HashMap<String, Option<String>>,
    },
}

fn txt_to_hash(text: &[String]) -> HashMap<String, Option<String>> {
    HashMap::from_iter(text.iter().filter_map(|s| txt_split(s)))
}

// String        ->  (String, Option<String>)
// "foo=bar"     ->  ("foo", Some("bar"))
// "foo=bar=baz" ->  ("foo", Some("bar=baz"))
// "foo"         ->  ("foo", None)
// ""            ->  None
fn txt_split(txt: &str) -> Option<(String, Option<String>)> {
    match txt.splitn(2, '=').collect::<Vec<_>>().as_slice() {
        [key, value] if !key.is_empty() => Some((key.to_string(), Some(value.to_string()))),
        [key] if !key.is_empty() => Some((key.to_string(), None)),
        _ => None,
    }
}

#[cfg(test)]
mod tests {
    use super::txt_split;
    #[test]
    fn txt_split_test() {
        assert_eq!(
            txt_split("foo=bar"),
            Some(("foo".to_string(), Some("bar".to_string())))
        );
        assert_eq!(
            txt_split("foo=bar=baz"),
            Some(("foo".to_string(), Some("bar=baz".to_string())))
        );
        assert_eq!(
            txt_split("foo="),
            Some(("foo".to_string(), Some("".to_string())))
        );
        assert_eq!(txt_split("foo"), Some(("foo".to_string(), None)));

        assert_eq!(txt_split(""), None);
        assert_eq!(txt_split("=baz"), None);
    }
}

// https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_messaging#app_side

fn read_object() -> Result<String, io::Error> {
    if USE_TEXT {
        let mut line = String::new();
        if io::stdin().read_line(&mut line)? == 0 {
            std::process::exit(0);
        }
        Ok(line.trim().to_string())
    } else {
        let mut count = [0_u8; 4];
        io::stdin().read_exact(&mut count)?;

        let mut buffer = vec![0_u8; u32::from_ne_bytes(count) as usize];
        io::stdin().read_exact(&mut buffer)?;

        Ok(serde_json::from_slice(&buffer).unwrap())
    }
}

fn write_object(s: &str) {
    if USE_TEXT {
        println!(
            "{} = {}",
            Local::now().format("%H:%M:%S.%6f").to_string(),
            s
        );
    } else {
        let count: u32 = s.len() as u32;
        io::stdout()
            .write_all(&count.to_ne_bytes())
            .expect("Write count");
        io::stdout().write_all(s.as_bytes()).expect("Write JSON");
        io::stdout().flush().expect("Flush");
    }
}

#[derive(std::cmp::PartialEq, std::cmp::Eq, std::hash::Hash)]
enum Change {
    Add(String),
    Remove(String),
}

#[derive(Default)]
struct Cache {
    query: String,
    cache: HashMap<String, Vec<(SystemTime, mdns::Record)>>,
    changes: HashSet<Change>,
}

// The cache needs to have:
// A hash of Vec<rr>:  MashMap<String, Vec<rr>>
// Each rr would be either a SRV or a TXT record.  (a PTR record?)
// Also need to have A records (AAAA records?)
// There is a timestamp, when each record was last updated.
// Maybe when each should be removed?

// When a record(set?) is added, pendiing add is created.
// When a record(set?) is removed, a pending remove is created.
// Maybe just call the pending routine immediately? As in keep the callback in the cache?

impl Cache {
    pub fn new(query: String) -> Self {
        Cache {
            query,
            ..Default::default()
        }
    }

    // So there are several conditions:
    // 1. Not in hash, (so no vec yet)
    //    insert new vec with record and time in hash
    // 2. In hash, !in vec
    //    add to vec with record and time
    //    Need to send "Added" to app (if both SRV+TXT in vec, along
    //    with A)
    // 3. In hash, in vec, ttl != 0
    //    Update time
    // 4. In hash, in vec, ttl == 0
    //    Remove from vec, if vec is empty, remove hash
    //    Need to send "Removed" to app
    //
    // Also, every N seconds, the whole hash and vec is scanned and if
    // any time has expired:
    //    Remove from vec, if vec is empty, remove hash
    //    Need to send "Removed" to app

    pub fn update(&mut self, now: SystemTime, record: mdns::Record) {
        if let RecordKind::PTR(_) | RecordKind::AAAA(_) = record.kind {
            return;
        }

        match self.cache.get_mut(&record.name) {
            // Case 1
            None => {
                if record.ttl > 0 {
                    if matches!(record.kind, RecordKind::SRV { .. })
                        && record.name.ends_with(&self.query)
                    {
                        self.changes.insert(Change::Add(record.name.clone()));
                    }
                    self.cache.insert(
                        record.name.clone(),
                        vec![(now + Duration::from_secs(record.ttl as u64), record)],
                    );
                }
            }
            // Case 2,3,4
            Some(records) => {
                match records
                    .iter()
                    .position(|(_, ref rr)| rr.kind == record.kind)
                {
                    // Case 2
                    None => {
                        if record.ttl > 0 {
                            if matches!(record.kind, RecordKind::SRV { .. })
                                && record.name.ends_with(&self.query)
                            {
                                self.changes.insert(Change::Add(record.name.clone()));
                            }
                            records.push((now + Duration::from_secs(record.ttl as u64), record));
                        }
                    }
                    // Case 3,4
                    Some(index) => {
                        match record.ttl {
                            // Case 4
                            0 => {
                                records.swap_remove(index);
                                if record.name.ends_with(&self.query) {
                                    self.changes.insert(Change::Remove(record.name));
                                }
                            }
                            // Case 3
                            _ => {
                                records[index] =
                                    (now + Duration::from_secs(record.ttl as u64), record);
                            }
                        }
                    }
                }
            }
        }
    }

    fn construct_message(&self, change: &Change) -> Option<ResponseJSON> {
        match change {
            Change::Add(name) => self.construct_added_message(name),
            Change::Remove(name) => self.construct_removed_message(name),
        }
    }

    fn construct_removed_message(&self, name: &str) -> Option<ResponseJSON> {
        Some(ResponseJSON::Remove {
            // "Some.Long.Name.._http._tcp.local"
            // "------ 3 ------|--2--|-1--|--0--"
            service: name.rsplitn(4, '.').nth(3)?.to_string(),
        })
    }

    // Return name's target and port
    fn name_to_srv(&self, name: &str) -> Option<(String, u16)> {
        for (_, rr) in self.cache.get(name)?.iter() {
            if let RecordKind::SRV {
                ref target, port, ..
            } = rr.kind
            {
                return Some((target.clone(), port));
            }
        }
        None
    }

    // Return name's txt values
    fn name_to_txt(&self, name: &str) -> Option<&[String]> {
        for (_, rr) in self.cache.get(name)?.iter() {
            if let RecordKind::TXT(ref txt) = rr.kind {
                return Some(txt);
            }
        }
        None
    }

    // Return name's ip address as string
    fn name_to_a(&self, name: &str) -> Option<String> {
        for (_, rr) in self.cache.get(name)?.iter() {
            if let RecordKind::A(a) = rr.kind {
                return Some(a.to_string());
            }
        }
        None
    }

    // Construct an "added" message
    fn construct_added_message(&self, name: &str) -> Option<ResponseJSON> {
        // "Some.Long.Name.._http._tcp.local"
        // "------ 3 ------|--2--|-1--|--0--"
        let parts: Vec<_> = name.rsplitn(4, '.').collect();
        let (hostname, port) = self.name_to_srv(name)?;

        Some(ResponseJSON::Add {
            service: parts.get(3)?.to_string(),
            srv: format!("{}.{}", parts.get(2)?, parts.get(1)?),
            hostname: hostname.to_owned(),
            port,
            address: self.name_to_a(hostname.as_str())?,
            txt: txt_to_hash(self.name_to_txt(name)?),
        })
    }

    pub fn send_queued_messages(&mut self, send: impl Fn(&str)) {
        for change in self.changes.iter() {
            if let Some(message) = self.construct_message(change) {
                send(serde_json::to_string(&message).unwrap().as_str())
            }
        }
        self.changes.clear();
    }

    pub fn queue_stale_records(&mut self) {
        let now = SystemTime::now();
        let mut stale = Vec::new();

        for (name, records) in self.cache.iter() {
            if name.ends_with(&self.query) {
                for (t, rr) in records.iter() {
                    if now > *t && matches!(rr.kind, RecordKind::SRV { .. }) {
                        stale.push(name.to_owned());
                        break;
                    }
                }
            }
        }

        for name in stale {
            self.changes.insert(Change::Remove(name.clone()));
            self.cache.remove(&name);
        }
    }
}

#[async_std::main]
async fn main() -> Result<(), Error> {
    // 0  app name
    // 1  complete path to manifest
    // 2  ID of addon that initiated

    // (0, "/usr/lib/mozilla/native-messaging-hosts/rust-mdns-discover")
    // (1, "/usr/lib/mozilla/native-messaging-hosts/mdns.discover.json")
    // (2, "mdns@exmaple.com")
    if false {
        use std::env;
        use std::fs::File;
        let mut file = File::create("/tmp/foo.txt")?;
        env::args().enumerate().for_each(|arg| {
            let _ = writeln!(file, "{:?}", arg);
        });
    }

    let (tx, rx) = bounded(1);
    let mut service_type = read_object()?;
    loop {
        let rx = rx.clone();
        let service_name = service_type.clone() + ".local";
        let result = async_std::task::spawn(async move { responder(&service_name, rx).await });
        service_type = read_object()?;
        let _ = tx.send(()).await;
        result.await?;
    }
}

async fn responder(service_name: &str, rx: async_channel::Receiver<()>) -> Result<(), Error> {
    let responses = mdns::discover::all(service_name, Duration::from_secs(55))?.listen();
    pin_mut!(responses);
    let mut cache = Cache::new(service_name.to_owned());
    let mut interval = stream::interval(Duration::from_secs(2));

    loop {
        select! {
            _ = rx.recv().fuse()               => break,
            _ = interval.next().fuse()         => cache.queue_stale_records(),
            response = responses.next().fuse() => if let Some(Ok(response)) = response {
                let now = SystemTime::now();
                for answer in response.records() {
                    cache.update(now, answer.to_owned());
                }
            },
        }
        cache.send_queued_messages(write_object);
    }
    Ok(())
}
