
TARGET := x86_64-pc-windows-gnu

APP := target/$(TARGET)/release/rust-mdns-discover.exe

all: $(APP)

$(APP): src/main.rs
	cargo build --release --target $(TARGET)

clean::
	cargo clean

# For testing on Linux
# USRDIR := $$HOME/.mozilla/native-messaging-hosts
# install-local: all mdns.discover.firefox.json.in
# 	mkdir -p $(USRDIR)
# 	sed -e "s|@DIR@|$(USRDIR)|" mdns.discover.firefox.json.in > $(USRDIR)/mdns.discover.json
# 	cp -f $(APP) $(USRDIR)/

# uninstall-local:
# 	$(RM) -f $(USRDIR)/mdns.discover.json
# 	$(RM) -f $(USRDIR)/rust-mdns-discover

# LIBDIR = /usr/lib/mozilla/native-messaging-hosts
# install: all mdns.discover.firefox.json.in
# 	mkdir -p $(LIBDIR)
# 	sed -e "s|@DIR@|$(LIBDIR)|" mdns.discover.firefox.json.in > $(LIBDIR)/mdns.discover.json
# 	cp -f $(APP) $(LIBDIR)/

# uninstall:
# 	$(RM) -f $(LIBDIR)/rust-mdns-discover
# 	$(RM) -f $(LIBDIR)/mdns.discover.json


installer: rust-mdns-discover.msi

rust-mdns-discover.msi: all mdns.discover.firefox.json.in rust-mdns-discover.wxs
	cp $(APP) .
	sed -e "s|@DIR@/rust-mdns-discover|rust-mdns-discover.exe|" mdns.discover.firefox.json.in > mdns.discover.json
	wixl -v rust-mdns-discover.wxs

clean::
	-rm rust-mdns-discover.msi mdns.discover.json *.exe
